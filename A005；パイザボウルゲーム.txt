<?php
// 1行目
$input_lines = trim(fgets(STDIN));
$input_lines = str_replace(array("\r\n","\r","\n"), '', $input_lines);
list($flame, $pin , $ball) = explode(' ',  $input_lines);
//2行目
$input = array();
$input = trim(fgets(STDIN));
$input = explode(" ", $input);

//処理開始
$score = 0;
for ($i = 0, $count = 1;$count < $flame; $count++){
    // ストライク
    if ($input[$i] == $pin){
        $score += $input[$i] + $input[$i+1] + $input[$i+2];
        $i++;
    }else{
    // スペア
    if ($input[$i] + $input[$i+1] == $pin){
        $score += $input[$i] + $input[$i+1] + $input[$i+2];
        $i += 2;
    }else{
    //普通のやつ
        $score += $input[$i] + $input[$i+1];
        $i += 2;
        }
    }
}
// echo $score . ' ';
    
//最終フレーム
$last_score = 0;
$last = $i; 

//1投目ストライク
if ($input[$last] == $pin){
    //1投目のスコアを足す
    $last_score += $input[$last] + $input[$last+1] + $input[$last+2];
    if($input[$last+1] == $pin) {
        //2投目もストライク(スペア)、そのスコアを足す
        $last_score += $input[$last+1] + $input[$last+2];
        //3投目の分も足して、ループを抜ける
        $last_score += $input[$last+2];
    }else{
         //3投目でスペア
        if ($input[$last] + $input[$last+1] == $pin){
            $last_score += $input[$last] + $input[$last+1] + $input[$last+2] + $input[$last + 2];
        }else{
        //ふつうのやつ
            $last_score += $input[$last+1] + $input[$last+2];
        }
    }
//1投目ストライクじゃないパターン
}else {
    //2投目スペア
    if($input[$last] + $input[$last+1] == $pin ){
       $last_score += $input[$last] + $input[$last+1]+$input[$last+2]+$input[$last+2];
    }else{
    //普通のやつ
    $last_score += $input[$last] + $input[$last+1];
    }
}
$score = $score + $last_score;
echo $score;
?>